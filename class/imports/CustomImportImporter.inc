<?php
/**
 * @file
 * Created by JetBrains PhpStorm.
 * User: Blobsmith
 * Date: 05/01/14
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */

interface CustomImportImporterInterface {

  /**
   * Get all ids of the already imported existing content.
   *
   * @return array
   *   array of ids
   */
  public function getExistingContentIds();

  /**
   * Get all the already imported existing content.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function getExistingContent($data, &$context);

  /**
   * Create or update each content from lines to import.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function updateContent($data, &$context);

  /**
   * Remove existing content if it was not created or updated.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function removeContent($data, &$context);
}


/**
 * Class CustomImportImporter
 *
 * $dataLinesToImport: All data lines to import.
 * $existingContentIds: Ids of content already imported.
 * $existingContent: Content already imported in Drupal.
 * $contentToBeDeleted: All content in this array should be deleted.
 * $contentMachineName: Drupal Machine name for content (ex: article, tags...).
 * $entityType: Drupal entity Type for content (ex: node, taxonomy_term...).
 * $disabledRemoveAction: If true: do not remove old imported content.
 * $startDate: Started date of importation.
 */
abstract class CustomImportImporter implements CustomImportImporterInterface {
  const IMPORTED_VALUE = '1';

  protected $dataLinesToImport = NULL;
  protected $existingContentIds = array();
  protected $existingContent = array();
  protected $contentToBeDeleted = array();
  protected $contentMachineName = NULL;
  protected $entityType = NULL;
  protected $disabledRemoveAction = FALSE;
  protected $startDate = NULL;

  /**
   * Constructor.
   *
   * @param string $content_machine_name
   *   Content type (bundle name)
   * @param array $lines_to_import
   *   List of data to import.
   */
  public function __construct($content_machine_name, $lines_to_import) {
    $this->startDate = format_date(time(), 'custom', 'Y-m-d H:i:s');
    $this->contentMachineName = $content_machine_name;
    $this->dataLinesToImport = $lines_to_import;

    if ($this->dataLinesToImport === NULL || empty($this->dataLinesToImport)) {
      $message = t('Import %contentMachineName: No data found.', array(
        '%contentMachineName' => $this->contentMachineName,
      ));
      throw new Exception($message);
    }
    CustomImportTools::attacheFields($this->entityType, $this->contentMachineName);
  }

  /**
   * Start the import without Drupal batch.
   *
   * @return array
   *   Logs of import.
   *
   * @throws Exception
   *   If error at import.
   */
  public function exec() {
    if ($this->dataLinesToImport === NULL || empty($this->dataLinesToImport)) {
      throw new Exception(t('Import %contentMachineName:  No data found.', array(
        '%contentMachineName' => $this->contentMachineName,
      )));
    }

    $logs = array();
    $this->getExistingContent(NULL, $logs);
    $this->updateContent(NULL, $logs);
    if (!$this->disabledRemoveAction) {
      $this->removeContent(NULL, $logs);
    }
    return $logs;
  }

  /**
   * Use for start a batch import.
   *
   * @return array
   *   List of operations.
   */
  public function getOperations() {
    $operations = $this->batchLoadingContent();
    $operations = array_merge($operations, $this->batchUpdatingContent());
    if (!$this->disabledRemoveAction) {
      $operations = array_merge($operations, $this->batchRemovingContent());
    }
    $operations = array_merge($operations, $this->batchEnding());
    return $operations;
  }

  /**
   * Get operations for existing content.
   *
   * @return array
   *   List of operations.
   */
  protected function batchLoadingContent() {
    $operations = array();
    $operations[] = array(
      'CustomImportLoader::batch',
      array(
        $this,
        'getExistingContent',
      ),
    );
    return $operations;
  }

  /**
   * Get operations for update content.
   *
   * @return array
   *   List of operations.
   */
  protected function batchUpdatingContent() {
    $operations = array();
    if ($this->dataLinesToImport) {
      $operations[] = array(
        'CustomImportLoader::batch',
        array(
          $this,
          'updateContent',
          NULL,
        ),
      );
    }
    return $operations;
  }

  /**
   * Get operations for removing content.
   *
   * @return array
   *   List of operations.
   */
  protected function batchRemovingContent() {
    $operations = array();
    $operations[] = array(
      'CustomImportLoader::batch',
      array(
        $this,
        'removeContent',
        NULL,
      ),
    );
    return $operations;
  }

  /**
   * Get operations for write logs.
   *
   * @return array
   *   List of operations.
   */
  protected function batchEnding() {
    $operations = array();
    $operations[] = array(
      'CustomImportLoader::batch',
      array(
        $this,
        'writeLogs',
        NULL,
      ),
    );
    return $operations;
  }

  /**
   * Write logs, call after importations.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   Context passed from Drupal batch
   */
  public function writeLogs($data, &$context) {
    $context['message'] = t('Writing log file');
    $import_key = get_class($this);
    $logger = new CustomImportLogger($import_key);
    foreach ($context['results']['messages'] as $line) {
      $logger->setLog($line);
    }
    $context['logFiles'][] = CustomImportLogger::getLogFile($import_key);

    if ($context['results']['created'] !== 0) {
      $logger->setLog(t('Number of created elements: %count', array(
          '%count' => $context['results']['created'],
        )));
    }

    if ($context['results']['updated'] !== 0) {
      $logger->setLog(t('Number of updated elements: %count', array(
        '%count' => $context['results']['updated'],
      )));
    }

    if ($context['results']['removed'] !== 0) {
      $logger->setLog(t('Number of removed elements: %count', array(
          '%count' => $context['results']['removed'],
        )));
    }

    $logger->finish(($context['results']['created'] + $context['results']['updated']), $context['results']['startDate']);
    unset($context['results']['messages']);
    unset($context['results']['startDate']);
    $context['results']['classList'][] = get_class($this);
    $context['finished'] = 1;
  }

  /**
   * Get existing content ids of last content imported.
   *
   * @return array|null
   *   Null or a list of ids.
   */
  public function getExistingContentIds() {
    $ids = NULL;
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType)
      ->entityCondition('bundle', $this->contentMachineName)
      ->fieldCondition(CustomImportTools::FIELD_IMPORTED, 'value', self::IMPORTED_VALUE, '=');
    $result = $query->execute();
    if (!empty($result)) {
      $ids = $result[$this->entityType];
    }
    return $ids;
  }

  /**
   * Get existing content of last content imported.
   *
   * @param array $data
   *   Array of ids to load.
   * @param array $context
   *   Context passed from Drupal batch
   */
  public function getExistingContent($data, &$context) {
    $this->existingContentIds = $this->getExistingContentIds();
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($this->existingContentIds);
    }
  }

  /**
   * Create or update each content from lines to import.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function updateContent($data, &$context) {
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($this->dataLinesToImport);
      $context['message'] = 'Creating or updating ' . $this->entityType;
    }

    if (!isset($context['results'])) {
      $context['results'] = array();
    }
    $context['results']['created'] = 0;
    $context['results']['updated'] = 0;
  }

  /**
   * Remove existing content if it was not created or updated.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function removeContent($data, &$context) {
    $info_entity = entity_get_info($this->entityType);
    $entity_id_name = $info_entity['entity keys']['id'];
    foreach ($this->existingContent as $content) {
      if (!isset($content->checked)) {
        $this->contentToBeDeleted[] = $content->{$entity_id_name};
      }
    }

    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($this->existingContent);
      $context['message'] = 'Removing terms';
    }

    $context['results']['removed'] = 0;
    $context['results']['startDate'] = $this->startDate;
  }

  /**
   * Disabled the remove action.
   *
   * @param bool $disabled
   *   If true, then disabled.
   */
  public function setDisabledRemoveAction($disabled) {
    $this->disabledRemoveAction = $disabled;
  }

  /**
   * Called before saving object to Drupal.
   *
   * @param StdClass $object
   *   An object (node, taxonomy...).
   * @param array $line
   *   List of data in a line to import.
   * @param mixed $line_number
   *   Number of the line.
   *
   * @return StdClass
   *   Modified $object.
   */
  abstract protected function beforeSave($object, $line, $line_number);

  /**
   * Save the object extract from $line to Drupal.
   *
   * @param StdClass $object
   *   An object (node, taxonomy...).
   * @param array $line
   *   List of data in a line to import.
   * @param mixed $line_number
   *   Number of the line.
   *
   * @return StdClass
   *   Modified $object.
   */
  abstract protected function save(&$object, $line, $line_number);

  /**
   * Called after saving object to Drupal.
   *
   * @param StdClass $object
   *   An object (node, taxonomy...).
   * @param array $line
   *   List of data in a line to import.
   * @param mixed $line_number
   *   Number of the line.
   *
   * @return StdClass
   *   Modified $object.
   */
  abstract protected function afterSave($object, $line, $line_number);

  /**
   * Get the line reference of the object to import.
   *
   * @param array $line
   *   List of data in a line to import.
   *
   * @return string
   *   Line reference.
   */
  abstract protected function getReference($line);

  /**
   * Get the language id for save to Drupal (en, fr...).
   *
   * @param array $line
   *   List of data in a line to import.
   *
   * @return string
   *   Code language.
   */
  abstract protected function getLanguage($line);
}
