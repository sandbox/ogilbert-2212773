<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 15/11/13
 * Time: 08:13
 */

abstract class CustomImportFileTask extends CustomImportTask {

  /**
   * File path for get content to import.
   *
   * @return string
   *   File path.
   */
  protected function getFilePath() {
    if (!isset(self::$settings['filePath'])) {
      throw new Exception(t('%class::getFilePath, A key "filePath" must be define in your setting file.', array(
        '%class' => get_class($this),
      )));
    }
    return self::$settings['filePath'];
  }
}
