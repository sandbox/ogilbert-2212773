<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 10.04.14
 * Time: 14:42
 */

class CustomImportLoader {
  const MENU_URL = 'admin/custom_imports';
  const CONTENT_LOADING_NUMBER = 300;
  protected static $batchObject = NULL;

  /**
   * Load a task class from task folder.
   *
   * @param string $task_class_name
   *   Name of class task.
   * @param string $action
   *   Method called in task object.
   */
  public static function taskLoader($task_class_name, $action = 'import') {
    try {
      $task = new $task_class_name();
      if ($action === 'checkService') {
        $task->checkService();
        drupal_set_message('Service checked', 'status');
      }
      else {
        $task->import();
      }
    }
    catch (Exception $e) {
      drupal_set_message('Import error: ' . $e->getMessage(), 'error');
    }
    drupal_goto('admin/custom_imports');
  }

  /**
   * Load settings from settings.php.
   *
   * @param string $class_name
   *   Name of a class to load settings.
   *
   * @return array
   *   Array of settings.
   */
  public static function loadSettings($class_name) {
    global $conf;
    if (isset($conf['custom_import']) && isset($conf['custom_import'][$class_name])) {
      return $conf['custom_import'][$class_name];
    }
    return array();
  }

  /**
   * Bridge to use Drupal batch in a object context.
   *
   * @param string $object
   *   Class name.
   * @param string $function
   *   Method name of a class.
   * @param array $data
   *   Data to set in first parameter of the method.
   * @param array $context
   *   Context set for Drupal batch.
   */
  public static function batch($object, $function, $data, &$context) {
    if (self::$batchObject === NULL || get_class($object) !== get_class(self::$batchObject)) {
      self::$batchObject = $object;
    }
    self::$batchObject->{$function}($data, $context);
  }
}
