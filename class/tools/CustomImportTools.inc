<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 28.11.13
 * Time: 16:11
 */

/**
 * Class CustomImportTools
 */
class CustomImportTools {
  const FIELD_REF = 'field_imported_ref';
  const FIELD_IMPORTED = 'field_imported';

  /**
   * Attach import fields in a type of Drupal entity (node, taxonomy_term...).
   *
   * @param string $entity_type
   *   Type of entity to attach fields.
   * @param string $content_type
   *   Bundle name of the entity.
   *
   * @throws Exception
   *   Send an exception if an error occurred.
   */
  public static function attacheFields($entity_type, $content_type) {
    $ref_info_instance = field_info_instance($entity_type, self::FIELD_REF, $content_type);
    $imported_info_instance = field_info_instance($entity_type, self::FIELD_IMPORTED, $content_type);

    // Attach field ref.
    if ($ref_info_instance === NULL) {
      $field_ref = self::createFieldsIfNeeded(self::FIELD_REF);
      if ($field_ref) {
        $ref_info_instance = self::createFieldsInstance($content_type, $field_ref['field_name'], 'Import Ref', 'reference field for import', $entity_type);
      }
    }

    // Attach field imported.
    if ($imported_info_instance === NULL) {
      $field_ref = self::createFieldsIfNeeded(self::FIELD_IMPORTED);
      if ($field_ref) {
        $imported_info_instance = self::createFieldsInstance($content_type, $field_ref['field_name'], 'Import Imported', 'imported field for import', $entity_type);
      }
    }

    if ($ref_info_instance === NULL || $imported_info_instance === NULL) {
      throw new Exception('Attache fields to node ' . $content_type . ' impossible');
    }
  }

  /**
   * Create a field in a Drupal instance.
   *
   * @param string $content_type
   *   Bundle name.
   * @param string $field_name
   *   Field name created.
   * @param string $field_label
   *   Field label createD.
   * @param string $field_description
   *   Field desciption createD.
   * @param string $entity_type
   *   Entity type of the bundle.
   * @param string $widget_type
   *   Widget type attached.
   * @param string $display_type
   *   Dusplay type attached.
   *
   * @return array
   *   The $instance array with the id property filled in.
   */
  public static function createFieldsInstance($content_type, $field_name, $field_label, $field_description, $entity_type = 'node', $widget_type = 'text_textfield', $display_type = 'text_default') {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $content_type,
      'label' => $field_label,
      'description' => t('%description', array('%description' => $field_description)),
      'settings' => array(
        'text_processing' => 0,
      ),
      'widget' => array('type' => $widget_type),
      'weight' => 11,
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => $display_type,
        ),
      ),
    );
    return field_create_instance($instance);
  }

  /**
   * Create a field if not exist.
   *
   * @param string $field_name
   *   Field name to create.
   *
   * @return mixed
   *   Field instance if created or null.
   */
  public static function createFieldsIfNeeded($field_name) {
    $field_exist = field_info_field($field_name);
    if ($field_exist === NULL) {
      $field = array(
        'field_name' => $field_name,
        'type' => 'text',
      );
      $field_exist = field_create_field($field);
    }
    return $field_exist;
  }

  /**
   * Get one node from its title.
   *
   * @param string $title
   *   Node title to search.
   * @param string $content_type
   *   Content type of node wanted.
   *
   * @return array
   *   Node found or empty array.
   */
  public static function getNodeByTitle($title, $content_type) {
    $data = array();
    $result = node_load_multiple(array(), array(
      'type' => $content_type,
      'title' => $title,
    ));
    if (!empty($result)) {
      $nids = array_keys($result);
      if (isset($nids[0])) {
        $data = array('target_id' => $nids[0]);
      }
    }
    return $data;
  }

  /**
   * Get one node from its old reference (imported ref).
   *
   * @param string $imported_ref
   *   Old content ref.
   * @param string $content_type
   *   Content type of node wanted.
   *
   * @return array
   *   Node found or empty array.
   */
  public static function getNodeByImportedRef($imported_ref, $content_type = NULL) {
    $node = NULL;
    $result = CustomImportTools::searchNode(array(
        array(
          'name' => self::FIELD_REF,
          'key' => 'value',
          'value' => $imported_ref,
        ),
      ),
      $content_type);
    if ($result) {
      $node = current($result);
    }
    return $node;
  }

  /**
   * Search nodes from fields.
   *
   * @param array $fields
   *   All conditions to find nodes.
   * @param string $content_type
   *   Content type of searched node.
   *
   * @return array
   *   All nodes found or false.
   */
  public static function searchNode($fields, $content_type = NULL) {
    $search = FALSE;

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');
    if ($content_type !== NULL) {
      $query->entityCondition('bundle', $content_type);
    }

    foreach ($fields as $field) {
      $query->fieldCondition($field['name'], $field['key'], $field['value']);
    }
    $result = $query->execute();
    if (!empty($result) && isset($result['node'])) {
      $search = $result['node'];
    }
    return $search;
  }

  /**
   * Get all taxonomy children from a parent tid or a term by condition.
   *
   * @param string $parent_tid
   *   Parent taxonomy id.
   * @param string $fied_name
   *   Name of field condition.
   * @param string $fied_value
   *   Value of field condition.
   * @param array $haystack
   *   List of terms name needed.
   *
   * @return array
   *   All children or a specific term.
   */
  public static function foundTaxonomyChild($parent_tid, $fied_name = NULL, $fied_value = '', $haystack = array()) {
    $terms = taxonomy_get_children($parent_tid);
    $term_found = NULL;
    foreach ($terms as $term) {
      if ($fied_name !== NULL) {
        $ref = field_get_items('taxonomy', $term, $fied_name);
        if ($ref !== FALSE && $ref[0]['value'] === $fied_value) {
          $term_found = $term;
          break;
        }
      }

      if (!empty($haystack)) {
        $name = $term->name;
        if (in_array($name, $haystack)) {
          $term_found = $term;
          break;
        }
      }
    }
    return $term_found;
  }

  /**
   * Load and register a file in Drupal.
   *
   * @param string $file_path
   *   Path to get the file content.
   * @param string $directory
   *   Directory to put file on disk.
   * @param string $file_name
   *   Name of the file to save. If not, the current file name is used.
   * @param bool $public
   *   False to put the file in the private Drupal directory.
   *
   * @return bool|null|stdClass
   *   The file if it was saved.
   */
  public static function loadFileFromUrl($file_path, $directory, $file_name = NULL, $public = TRUE) {
    $file = NULL;
    $drupal_directory = 'public';
    if (!$public) {
      $drupal_directory = 'private';
    }
    $drupal_directory .= '://' . $directory;

    if ($file_path) {
      // Load the files contents.
      $file_content = @file_get_contents($file_path);

      if ($file_content) {
        // Make directory if needed.
        if (!file_prepare_directory($drupal_directory)) {
          drupal_mkdir($drupal_directory, 0777, TRUE);
        }

        if ($file_name === NULL || $file_name === '') {
          $file_name = basename($file_path);
        }

        // Register file in drupal db.
        if ($file_name !== '') {
          $file = file_save_data($file_content, $drupal_directory . '/' . $file_name, FILE_EXISTS_REPLACE);
        }
      }
    }
    return $file;
  }
}
