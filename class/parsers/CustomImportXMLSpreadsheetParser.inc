<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 04/04/14
 * Time: 11:08
 */

abstract class CustomImportXMLSpreadsheetParser extends CustomImportParser {

  /**
   * This function define how to parse data for each specific type of data.
   *
   * @return array
   *   All data parsed in $this->parsed_data.
   */
  protected function extractData() {
    if (empty($this->data)) {
      throw new Exception(t('No data can be found'));
    }

    $lines = simplexml_load_string($this->data, "SimpleXMLElement", LIBXML_NOCDATA);
    if ($lines === FALSE) {
      throw new Exception(t('Data format invalid.'));
    }

    $header = array();
    $first = TRUE;
    foreach ($lines->Worksheet[0]->Table->Row as $line) {
      $tmp_data = array();
      $index = 0;
      foreach ($line->Cell as $cell) {
        if ($first) {
          $header[$index] = (string) $cell->Data;
        }
        else {
          $tmp_data[$header[$index]] = (string) $cell[0]->Data;
        }
        $index++;
      }
      if (!$first) {
        $this->parsedData[] = $tmp_data;
      }
      $first = FALSE;
    }
  }
}
