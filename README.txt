********************************************************************************
*                               CUSTOM IMPORT MODULE
********************************************************************************

All class you can use are in the class folder
    class/imports : contain all types of import (ex: import node, taxonomy...)
    class/parsers : contain all types of parser (ex: excel spreadsheet parser,
     xml parser, idx parser...)
    class/task : contain all types of task (ex: import from files, mysql db...)
    class/tools : useful functions for import
    class/restructuring : Not available yet

********************************************************************************
*                               HOW TO USE
********************************************************************************
Create a module: (ex: mymodule)


DON'T FORGET TO CREATE A NEW MODULE, do not working in this module !!!!

- Step 1: Create a menu in mymodule.module
    /**
     *
     * hook_menu
     *
     */
    function mymodule_menu() {
        $items = array();
        $items['admin/custom_imports/mymenu'] = array(
            'title' => t('Title you want'),
            'page callback' => 'CustomImportLoader::taskLoader',
            'page arguments' => array('MyTaskClassName'),
            'access callback' => 'user_access',
            'access arguments' => array('administer nodes'),
            'type' => MENU_NORMAL_ITEM|MENU_LOCAL_TASK,
        );
        return $items;
    }
- Step 2: Create a task (ex:MyTaskClassName)
    Create an import task or extends CustomImportTask to create your own task
    Implement all abstract methods

    The task class you created must be call from the previous menu
    (in the page arguments parameter)

    *You can also just check data
    Replace 'page arguments' => array('MyTaskClassName'),
    By 'page arguments' => array('MyTaskClassName', 'checkService'),

- Step 3: Create your own parser
    Extends an existing parser like "CustomImportCSVParser"
    Implement all abstract methods

- Step 4: Create an importer
    * Create an importer by extends an existing importer like
    "CustomImportNodeImporter" (If you want to import node)
    or "CustomImportTaxonomyImporter"
    * Implement all abstract methods

- Step 5: Add your import settings in the setting file: (see an example below)
    $conf['custom_import'] = array(             //module custom import settings
        'CustomImportExampleXMLTask' => array(  //class name to set the settings
            'filePath' => 'public://import/monthToTags.xml',    //path to file
            'bundleName' => array('tags'),      //Content machine name for
                                                //import content in node basic
                                                //page and taxonomy tags
            'memoryLimit' => '128M'             //memory usage
        )
    );

- For more detail about implementation:
    * You can install the module cutom_import_example
      (see the cutom_import/cutom_import_example/readme.txt file)
    * If you don't want to use Drupal batch, you can see 2 examples of
      implementation in unit test folder:
        custom_import/tests/node -> import nodes from csv file
        custom_import/tests/taxonomy -> import taxonomy from csv file
